﻿#include "form.h"
#include "ui_form.h"
#include<QDebug>
#include<iostream>
#include<string>
#include <QTimer>
#include <QDateTime>
Form::Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);
    writed_rows = 0;
    CreateView();
}

Form::~Form()
{
    delete ui;
}
void Form::CreateView()
{
    mainLayout = new QVBoxLayout;       //垂直布局
    mainLayout->setSpacing(10);         //设置控件间距
    mainLayout->setMargin(10);          //设置边缘间距
    testBtn = new QPushButton("Test");
    //添加QTableView代码
    tableView = new QTableView;
    standItemModel = new QStandardItemModel();

    //添加表头
    standItemModel->setColumnCount(6);
    standItemModel->setHeaderData(0,Qt::Horizontal,QStringLiteral("序号"));   //设置表头内容
    standItemModel->setHeaderData(1,Qt::Horizontal,QStringLiteral("名字"));
    standItemModel->setHeaderData(2,Qt::Horizontal,QStringLiteral("学号"));
    standItemModel->setHeaderData(3,Qt::Horizontal,QStringLiteral("班级"));
    standItemModel->setHeaderData(4,Qt::Horizontal,QStringLiteral("签到时间"));
    standItemModel->setHeaderData(5,Qt::Horizontal,QStringLiteral("是否迟到"));

    for(int i = 1;i<=100;i++)
    {

        QString str = QString::number(i-9);

        QStandardItem *standItem1 = new QStandardItem(str);
//        QStandardItem *standItem2 = new QStandardItem("田俊康");
        standItemModel->setItem(i,0,standItem1);                                //表格第i行，第0列添加一项内容
        standItemModel->item(i,0)->setForeground(QBrush(QColor(255,0,0)));      //设置字符颜色
        standItemModel->item(i,0)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

//        standItemModel->setItem(i,1,standItem2);                                //表格第i行，第1列添加一项内容
//        standItemModel->item(i,1)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中
    }

    tableView->setModel(standItemModel);    //挂载表格模型
    //设置表格属性
    tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignCenter);        //表头信息显示居中
    tableView->horizontalHeader()->setSectionResizeMode(0,QHeaderView::Fixed);  //设定表头列宽不可变
    tableView->horizontalHeader()->setSectionResizeMode(1,QHeaderView::Fixed);
//    tableView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);//设定第2列表头弹性拉伸
    tableView->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Fixed);//设定第2列表头弹性拉伸
    tableView->setColumnWidth(0,100);       //设定表格第0列宽度
    tableView->setColumnWidth(1,100);
    tableView->setColumnWidth(2,100);       //设定表格第0列宽度
    tableView->setColumnWidth(3,100);
    tableView->setColumnWidth(4,300);       //设定表格第0列宽度
    tableView->setColumnWidth(5,100);
    tableView->verticalHeader()->hide();    //隐藏默认显示的行头
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows); //设置选中时整行选中
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);  //设置表格属性只读，不能编辑
//    tableView->setContextMenuPolicy(Qt::CustomContextMenu);         //需要在表格使用右键菜单，需要启动该属性
//    tableView->sortByColumn(0,Qt::AscendingOrder);                 //表格第0列，按降序排列
    standItemModel->removeRows(0,10);                               //删除从第0行开始的连续10行
    mainLayout->addWidget(testBtn);
    mainLayout->addWidget(tableView);    //添加控件
    this->setLayout(mainLayout);        //显示垂直布局

}

void Form::add_sign_data(Person person,QString ee)
{
    QStandardItem *standItem1 = new  QStandardItem(person.name_cn);   //名字
    QStandardItem *standItem2 = new  QStandardItem(person.id_num);
    QStandardItem *standItem3 = new  QStandardItem("自动化");

    QDateTime cur_date_time = QDateTime::currentDateTime();
    QString date_time = cur_date_time.toString ("hh:mm:ss");

    QStandardItem *standItem4 = new  QStandardItem(date_time);
    QDateTime curDateTime = QDateTime::currentDateTime();
    QString str = curDateTime.toString ("hh:mm:ss");
    QStringList strL1 = str.split(":");
    QStringList strL2 = ee.split(":");
    QString flag;
    int t1 = strL1[0].toInt()*60+strL1[1].toInt();
    int t2 = strL2[0].toInt()*60+strL2[1].toInt();
    if( t1 <=  t2)
    {
        flag = "否";
    }
    else
    {
        flag = "是";
    }
    //qDebug()<<t1<<t2;
    QStandardItem *standItem5 = new  QStandardItem(flag);
    standItemModel->setItem(writed_rows,1,standItem1);
    standItemModel->item(writed_rows,1)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

    standItemModel->setItem(writed_rows,2,standItem2);
    standItemModel->item(writed_rows,2)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

    standItemModel->setItem(writed_rows,3,standItem3);
    standItemModel->item(writed_rows,3)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

    standItemModel->setItem(writed_rows,4,standItem4);
    standItemModel->item(writed_rows,4)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

    standItemModel->setItem(writed_rows,5,standItem5);
    standItemModel->item(writed_rows,5)->setTextAlignment(Qt::AlignCenter);           //设置表格内容居中

    writed_rows++;

}

void Form::closeEvent(QCloseEvent *event)
{
    int flag = 1;
    emit send_time_start(flag);
}
