﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include <opencv2/opencv.hpp>
#include "face_model.h"
#include "administor.h"
#include "form.h"

using namespace cv;


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    QImage mat2qim(Mat & mat)
    {
        cvtColor(mat, mat, COLOR_BGR2RGB);
        QImage qim((const unsigned char*)mat.data, mat.cols, mat.rows, mat.step,
            QImage::Format_RGB888);
        return qim;
    }
public:
    FaceDetector *face_detector;
    bool start_detect;
    bool stop_detect;

private slots:
    void on_open_cam_clicked();
    void read_frame();
    void on_admin_clicked();

    void on_show_excel_clicked();

    void on_take_pic_clicked();


    void receive_data(Person person);
    void receive_flag(int flag);
private:
    Ui::Widget *ui;
    QTimer    *timer;
    QImage    *imag;
    VideoCapture cap;

    bool cam_flag;
    administor *admin_set;
    Form *form_set;
};
#endif // WIDGET_H
