﻿#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>
#include <QFileDialog>
#include <QWidget>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_btn_choice_clicked()
{
    QString s = QFileDialog::getOpenFileName(
                    this, "选择人脸图片",
                    "/",
                    "图片文件 (*.jpg);; 所有文件 (*.*);; ");
        //qDebug() << "path=" << s;
        if (!s.isEmpty())
        {
            ui->pic_edit->setText(s);
        }
}

void Dialog::on_btn_ok_clicked()
{
    if(ui->pic_edit->text() != "")
        pic_path = ui->pic_edit->text();
    if(ui->name_cn_edit->text() != "")
        name_cn = ui->name_cn_edit->text();
    if(ui->name_eng_edit->text() != "")
        name_eng = ui->name_eng_edit->text();
    if(ui->class_edit->text() != "")
        class_name = ui->class_edit->text();
    if(ui->id_num_edit->text() != "")
        id_num = ui->id_num_edit->text();

    QString exe_path =  QCoreApplication::applicationDirPath();
    QString fileName =  exe_path + "/data/face_info1.txt";//选择路径
    if (fileName.isEmpty())     //如果未选择文件便确认，即返回
        return;
    QString write_txt = name_cn+" "+name_eng+" "+id_num+ " ";
    QFile file(fileName);
    pic_path = pic_path.split("/")[pic_path.split("/").size()-1];
       if (file.open(QIODevice::ReadWrite | QIODevice::Text))
       {
           QTextStream stream(&file);
           stream.seek(file.size());

           stream <<"\n"<< write_txt+"/"+pic_path << " ";
           file.close();
       }
}

void Dialog::on_btn_cancel_clicked()
{

}
