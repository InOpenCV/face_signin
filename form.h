﻿#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <admin.h>
#include <QVBoxLayout>
#include <QPushButton>
#include <QStandardItem>
#include <QStandardItemModel>
#include <QTreeView>
#include <QTableView>
#include <QHeaderView>
#include "face_model.h"
#include <QCloseEvent>
namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();
    void CreateView();
    void add_sign_data(Person person,QString ee);
//void add_sign_data();

    QPushButton *testBtn;
    QVBoxLayout *mainLayout;
   //    QTreeView *treeView;
    QStandardItemModel *standItemModel;
    QTableView *tableView;
signals:
    void send_time_start(int flag);
protected:
     void closeEvent(QCloseEvent *event);
private:
    Ui::Form *ui;

    int writed_rows;
//    QPushButton *testBtn;
//    QVBoxLayout *mainLayout;
//   //    QTreeView *treeView;
//    QStandardItemModel *standItemModel;
//    QTableView *tableView;


};

#endif // FORM_H
