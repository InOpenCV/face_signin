﻿#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QDebug>
#include <QCoreApplication>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    timer   = new QTimer(this);
    imag    = new QImage();
    form_set = new Form();
    admin_set = new administor();
    connect(timer, SIGNAL(timeout()), this, SLOT(read_frame()));
    connect(admin_set, SIGNAL(send_data(Person)), this, SLOT(receive_data(Person)));
    connect(admin_set, SIGNAL(send_time_start(int)), this, SLOT(receive_flag(int)));
    connect(form_set, SIGNAL(send_time_start(int)), this, SLOT(receive_flag(int)));
    face_detector = new FaceDetector;
    cam_flag = false;
    start_detect =stop_detect = false;
    timer->start(5);
}
Widget::~Widget()
{
    delete ui;
}


void Widget::on_open_cam_clicked()
{

    cap.open(0);
    cam_flag = true;
}
void Widget::read_frame()
{
    if(cam_flag)
    {
        Mat frame;
        cap >>frame;
        Person person_temp;
        if(start_detect)
        {
            bool sign_flag = face_detector->recog_face(frame,person_temp);
            if(sign_flag)
                form_set->add_sign_data(person_temp,admin_set->end_time);
        }
        cv::resize(frame,frame,cv::Size(400,350));
        QImage image =mat2qim(frame);
        //imshow("q",frame);

        ui->label_show_img->setPixmap(QPixmap::fromImage(image));
    }
    QDateTime curDateTime = QDateTime::currentDateTime();
    ui->label_time->setText (curDateTime.toString ("hh:mm:ss"));
}

void Widget::on_admin_clicked()
{
    timer->stop();
    admin_set->show();
}

void Widget::on_show_excel_clicked()
{
    timer->stop();
    stop_detect = true;
    form_set->resize(760,640);
    form_set->setWindowTitle(QString::fromUtf8("签到表"));
    form_set->show();
}

void Widget::on_take_pic_clicked()
{
    if(stop_detect == true)
    {
         stop_detect = false;
    }
    else
    {
        //face_detector->add_face_batch(face_detector->pic_dir_path);
    }
    start_detect = true;
    timer->start(10);
}

void Widget::receive_data(Person person)
{
    //接收新添加的信息并更新人脸库
    face_detector->add_face(person);
}
void Widget::receive_flag(int flag)
{
   if (flag == 1)
        timer->start(10);
}
